/*showroom EV interlocking 16F886*/

#include <xc.h>
#define _XTAL_FREQ 8000000
#pragma config BOREN = OFF
#pragma config FOSC = INTRC_NOCLKOUT
#pragma config FCMEN = OFF
#pragma config MCLRE = OFF
#pragma config WDTE = OFF
#pragma config LVP = OFF
#pragma config PWRTE = ON

#define timerH cf
#define timerL 2c
#define updownBZcounter 4                               //up down buzzer counter
#define updownLEDcounter 100                            //up down LED counter


int mode;                                               //mode
    //mode 0:standby
    //mode 1:up down ON
int udc;                                                //up down counter



/*main setting*/
void main(void)
{
////input////
//RB0:up sw
//RB1:down sw
//RB2:ccu output

////output////
//RC5:down LED
//RC7:up LED
//RA7:buzzer

OSCCON=0b01110000;                                      //clock 8MHz
CM1CON0=0b00000000;                                     //comparator OFF
PORTA=0b00000000;                                       //portA output 0
PORTB=0b00000000;                                       //portB output 0
PORTC=0b00000000;                                       //portC output 0
TRISA=0b00000000;                                       //portA noinput
TRISB=0b00000111;                                       //portB input RB0,1,2
TRISC=0b00000000;                                       //portC input -

ANSEL=0b00000000;                                       //nothing analog
ANSELH=0b00000000;                                      //nothing analog

WPUB=0b00000111;                                        //RB0,1,2 internal pull up
OPTION_REG=0b00000000;                                  //pull up enable

/*timer interrupt 1 setting*/
T1CON=0x30;                                             //interval timer
TMR1H=0xtimerH;                                         //timer H
TMR1L=0xtimerL;                                         //timer L
TMR1IE=1;                                               //interrupt ok
TMR1ON=1;                                               //timer1 start

PEIE=1;                                                 //all interrupt ok
GIE=1;                                                  //all interrupt ok

/*default*/
mode=0;                                                 //mode
udc=0;                                                  //up down counter

///////////////////////main loop//////////////////////////
while(1)                                                //main loop
    {
    }
}
/////////////////////timer interrupt//////////////////////
void interrupt A(void)
{
    if(TMR1IF)
        {
        TMR1H=0xtimerH;                                 //timer H
        TMR1L=0xtimerL;                                 //timer L
        }

////////mode decision////////
    switch(mode)                                        //switch(mode) start
        {
        case 0:                                         //if mode 0
               if(RB2==0)                               //if ccu output ON
                    {
                    mode=1;                             //mode -> sw waiting
                    }
               break;                                   //mode 0 break
        case 1:                                         //if mode 1
               break;                                  //mode 1 break
        }

////////LED buzzer drive////////
    switch(mode)                                        //switch(mode) start
        {
        case 1:
                if(udc>=updownLEDcounter)               //if updown LED counter timeout
                    {
                    RC5=0;                              //up LED OFF
                    RC7=0;                              //down LED OFF
                    RA7=0;                              //buzzer OFF
                    udc=0;                              //updown counter reset
                    mode=0;                             //mode 0
                    }
                else
                    {
                    if(udc>=updownBZcounter)             //if updown BZ counter timeout
                        {
                        RC5=1;                           //up LED ON
                        RC7=1;                           //down LED ON
                        RA7=0;                           //buzzer OFF
                        udc++;                           //up counter ++
                        }
                    else
                        {
                        RC5=1;                           //up LED ON
                        RC7=1;                           //down LED ON
                        RA7=0;                           //buzzer OFF
                        udc++;                           //up counter ++
                        }
                    }
                break;                                   //mode 4 break
        default:
                break;                                   //mode other break
        }
TMR1IF=0;                                                //timer interrupt flag clear
}
